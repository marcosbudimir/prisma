﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Competidores.Api.Data;
using Competidores.Api.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Competidores.Api.Controllers
{
    [Route("api/marca")]
    public class MarcaController : Controller
    {
        private readonly DataContext _context;

        public MarcaController(DataContext context)
        {
            _context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var marcas = new List<Marca>();
            await Task.Run(() =>
            {
                marcas = _context.Marcas.ToList();
            });
            return Ok(marcas);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var competidor = await _context.Competidores.FindAsync(id);
            return Ok(competidor);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
