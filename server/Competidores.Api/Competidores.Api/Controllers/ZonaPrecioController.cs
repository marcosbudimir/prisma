﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Competidores.Api.Data;
using Competidores.Api.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Competidores.Api.Controllers
{
    [Route("api/zonaprecios")]
    public class ZonaPrecioController : Controller
    {
        private readonly DataContext _context;

        public ZonaPrecioController(DataContext context)
        {
            _context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var zonaPrecio = new List<ZonaPrecios>();
            await Task.Run(() =>
            {
                zonaPrecio = _context.ZonasPrecios.ToList();
            });
            return Ok(zonaPrecio);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post(string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
