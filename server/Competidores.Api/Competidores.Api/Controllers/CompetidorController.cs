﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Competidores.Api.Data;
using Competidores.Api.Data.DTOs;
using Competidores.Api.Models;
using Microsoft.AspNetCore.Mvc;


namespace Competidores.Api.Controllers
{
    [Route("api/competidor")]
    public class CompetidorController : Controller
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CompetidorController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var competidores = new List<CompetidorDto>();
             await Task.Run(() =>
             {
                 competidores = _mapper.Map<List<CompetidorDto>>(_context.Competidores.ToList());
             });
            return Ok(competidores);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var competidor = await _context.Competidores.FindAsync(id);
            return Ok(competidor);
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CompetidorDto competidor)
        {
            try
            {
                await _context.Competidores.AddAsync(
                _mapper.Map<Competidor>(competidor));
                await _context.SaveChangesAsync();
                return Created(new Uri("list"), true);
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var competidor = await _context.Competidores.FindAsync(id);
            await Task.Run(() =>
            {
                _context.Competidores.Remove(competidor);
            });
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
