﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competidores.Api.Models
{
    public class Marca
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
