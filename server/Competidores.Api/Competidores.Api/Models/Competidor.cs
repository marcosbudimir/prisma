﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competidores.Api.Models
{
    public class Competidor
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public int MarcaId { get; set; }
        public string NombreCorporativo { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public bool Marcador { get; set; }
        public bool Observar { get; set; }
        public int ZonaPreciosId { get; set; }

        public ZonaPrecios ZonaPrecios { get; set; }
        public Marca Marca { get; set; }
    }
}
