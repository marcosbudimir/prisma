﻿using AutoMapper;
using Competidores.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competidores.Api.Data.DTOs
{
    public class ConfigDto : Profile
    {
        public ConfigDto()
        {
            CreateMap<Competidor, CompetidorDto>()
                .ForMember(d => d.MarcaNombre, o => o.MapFrom(s => s.Marca.Nombre))
                .ForMember(d => d.ZonaPreciosNombre, o => o.MapFrom(s => s.ZonaPrecios.Nombre));

            CreateMap<CompetidorDto, Competidor>();
        }
    }
}
