﻿using Competidores.Api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competidores.Api.Data
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> dbContextOptions)
            :base(dbContextOptions){}

        public DbSet<Competidor> Competidores { get; set; }
        public DbSet<Marca> Marcas { get; set; }
        public DbSet<ZonaPrecios> ZonasPrecios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Marca>().HasData(
                new Marca
                {
                    Id = 1,
                    Nombre = "Jumbo"
                },
                new Marca
                {
                    Id = 2,
                    Nombre = "Walmart"
                },
                new Marca
                {
                    Id = 3,
                    Nombre = "Carrefour"
                },
                new Marca
                {
                    Id = 4,
                    Nombre = "Dia"
                }
            );

            modelBuilder.Entity<ZonaPrecios>().HasData(
                new ZonaPrecios
                {
                    Id = 1,
                    Nombre= "NEA"
                },
                new ZonaPrecios
                {
                    Id = 2,
                    Nombre = "NOA"
                },
                new ZonaPrecios
                {
                    Id = 3,
                    Nombre = "Cuyo"
                },
                new ZonaPrecios
                {
                    Id = 4,
                    Nombre = "Centro"
                },
                new ZonaPrecios
                {
                    Id = 5,
                    Nombre = "Patagonia"
                }
                );
        }
    }
}
