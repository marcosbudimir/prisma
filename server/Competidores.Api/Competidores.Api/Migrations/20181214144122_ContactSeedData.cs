﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Competidores.Api.Migrations
{
    public partial class ContactSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Marcas",
                columns: new[] { "Id", "Nombre" },
                values: new object[,]
                {
                    { 1, "Jumbo" },
                    { 2, "Walmart" },
                    { 3, "Carrefour" },
                    { 4, "Dia" }
                });

            migrationBuilder.InsertData(
                table: "ZonasPrecios",
                columns: new[] { "Id", "Nombre" },
                values: new object[,]
                {
                    { 1, "NEA" },
                    { 2, "NOA" },
                    { 3, "Cuyo" },
                    { 4, "Centro" },
                    { 5, "Patagonia" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Marcas",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Marcas",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Marcas",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Marcas",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ZonasPrecios",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ZonasPrecios",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ZonasPrecios",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ZonasPrecios",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ZonasPrecios",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
