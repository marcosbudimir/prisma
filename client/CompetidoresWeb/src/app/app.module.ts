import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CompetidorListComponent } from './pages/competidor-list/competidor-list.component';
import { CompetidorCreateComponent } from './pages/competidor-create/competidor-create.component';
import {
  MatTableModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatSelectModule,
  MatIconModule,
  MatDialogModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule, GoogleMapsScriptProtocol } from '@agm/core';
import { CompetidorConfirm } from './pages/competidor-list/competidor-confirm';

const routes: Routes = [
  {path: '', component: CompetidorListComponent},
  {path: 'competidor', component: CompetidorListComponent},
  {path: 'create', component: CompetidorCreateComponent},
  {path: '**', component: CompetidorListComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CompetidorListComponent,
    CompetidorCreateComponent,
    CompetidorConfirm
  ],
  entryComponents: [
    CompetidorConfirm
  ],
  imports: [
        RouterModule.forRoot(routes),
        BrowserAnimationsModule,
        MatTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        MatCheckboxModule,
        MatSelectModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatIconModule,
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyDLl42BLbrZmGgpjzNRATjsUjOJAgl3kjA',
          libraries: ["places"],
          protocol: GoogleMapsScriptProtocol.AUTO,
          // hostAndPath: 'localhost:14368/api/mapa/js'
        })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
