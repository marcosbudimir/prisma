import { Component, OnInit, Inject } from '@angular/core';
import { CompetidoresService } from 'src/app/services/competidores.service';
import { Competidor } from '../../models/competidor';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CompetidorConfirm } from './competidor-confirm';
import swal from 'sweetalert';

@Component({
  selector: 'app-competidor-list',
  templateUrl: './competidor-list.component.html',
  styleUrls: ['./competidor-list.component.css']
})
export class CompetidorListComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'direccion', 'codigo', 'nombreCorporativo', 'eliminar'];
  competidores: Competidor[];
  constructor(private _competidorSvc: CompetidoresService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.load();
  }

  load() {
    this._competidorSvc.get().subscribe((list) => {
      this.competidores = list;
    });
  }

  delete(competidor: Competidor) {
    const dialogRef = this.dialog.open(CompetidorConfirm, {
      width: '250px',
      data: competidor
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._competidorSvc.delete(result).subscribe(() => {
          swal('eliminar competidor', 'se elimino de manera exitosa', 'success');
          this.load();
        }
        );
      }
    });
  }
}

