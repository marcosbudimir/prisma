import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Competidor } from 'src/app/models/competidor';

@Component({
    selector: 'app-competidor-confirm',
    templateUrl: './competidor-confirm.html',
  })
  export class CompetidorConfirm {
  
    constructor(
      public dialogRef: MatDialogRef<CompetidorConfirm>,
      @Inject(MAT_DIALOG_DATA) public data: Competidor) {}
  
    Cancelar(): void {
      this.dialogRef.close();
    }
  }
  