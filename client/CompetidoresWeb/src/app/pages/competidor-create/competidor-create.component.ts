import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Competidor } from '../../models/competidor';
import { CompetidoresService } from '../../services/competidores.service';
import { Router } from '@angular/router';
import { MarcasService } from '../../services/marcas.service';
import { Marca } from '../../models/marca';
import { ZonaPrecios } from '../../models/zonaPrecios';
import { ZonaPreciosService } from '../../services/zona-precios.service';

@Component({
  selector: 'app-competidor-create',
  templateUrl: './competidor-create.component.html',
  styleUrls: ['./competidor-create.component.css']
})
export class CompetidorCreateComponent implements OnInit {
  competidorForm: FormGroup;
  errorMsg = '';
  lat = -27.450000762939;
  lng = -58.983299255371;

  constructor(private _competidorSvc: CompetidoresService,
    private _marcaSvc: MarcasService,
    private _zonaPreciosSvc: ZonaPreciosService,
    private router: Router) { }
    marcas: Marca[];
    zonasPrecios: ZonaPrecios[];
  ngOnInit() {
    this.competidorForm = new FormGroup({
      codigo : new FormControl(null, Validators.required),
      nombre : new FormControl(null, Validators.required),
      direccion: new FormControl(null, Validators.required),
      nombreCorporativo: new FormControl(null, Validators.required),
      marcador: new FormControl(null, Validators.required),
      observar: new FormControl(null, Validators.required),
      marca: new FormControl(null, Validators.required),
      zonaPrecio: new FormControl(null, Validators.required)
    });
    this._marcaSvc.get().subscribe((data) => {
      this.marcas = data;
    });
    this._zonaPreciosSvc.get().subscribe((data) => {
      this.zonasPrecios = data;
    });
  }

  async create() {
    const competidor = new Competidor();
    competidor.codigo = this.competidorForm.value.codigo;
    competidor.nombre = this.competidorForm.value.nombre;
   competidor.direccion = this.competidorForm.value.direccion;
    competidor.nombreCorporativo = this.competidorForm.value.nombreCorporativo;
   competidor.marcador = this.competidorForm.value.marcador;
    competidor.observar = this.competidorForm.value.observar;
   competidor.latitud = this.lat;
    competidor.longitud = this.lng;
     competidor.marcaId = this.competidorForm.value.marca.id;
    competidor.zonaPreciosId = this.competidorForm.value.zonaPrecio.id;

    this._competidorSvc.create(competidor).subscribe(resp => {
      swal('crear competidor', 'se guardo de manera exitosa', 'success');
      this.router.navigate(['/competidor']);
    }, error => this.errorMsg = 'something was wrong, Verify and try again');
  }

  updatePosition($event) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
  }
}
