export class Competidor {
    id: number;
    codigo: number;
    nombre: string;
    direccion: string;
    marcaId: number;
    marcaNombre: string;
    nombreCorporativo: string;
    latitud: number;
    longitud: number;
    marcador: boolean;
    observar: boolean;
    zonaPreciosId: number;
    zonaPreciosNombre: string;
}
