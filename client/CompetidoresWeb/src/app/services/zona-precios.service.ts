import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ZonaPrecios } from '../models/zonaPrecios';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ZonaPreciosService {
  private url = environment.apiUrl + 'zonaPrecios';
  constructor(private http: HttpClient) { }
  get() {
    return new Observable<ZonaPrecios[]>(obs => {
      this.http.get<ZonaPrecios[]>(this.url).subscribe(
        value => obs.next(value),
        e => obs.error(e)
        );
    });
  }
}
