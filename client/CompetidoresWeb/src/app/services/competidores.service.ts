import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Competidor } from '../models/competidor';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompetidoresService {

  private url = environment.apiUrl + 'competidor';
  constructor(private http: HttpClient) { }
  _headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  create(competidor: Competidor) {
    return this.http.post(this.url, competidor,
    { headers: this._headers}).pipe(
    map((resp: any) => {
      return resp;
    }));
  }

  get() {
    return new Observable<Competidor[]>(obs => {
      this.http.get<Competidor[]>(this.url).subscribe(
        value => obs.next(value),
        e => obs.error(e)
        );
    });
  }

  getById(id: number) {
    return this.http.get(this.url + id);
  }

  delete(id: number) {
    const urlDelete = `${this.url}/${id}`;
    return this.http.delete(urlDelete);
  }
}
