import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Marca } from '../models/marca';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MarcasService {
  private url = environment.apiUrl + 'marca';
  constructor(private http: HttpClient) { }
  get() {
    return new Observable<Marca[]>(obs => {
      this.http.get<Marca[]>(this.url).subscribe(
        value => obs.next(value),
        e => obs.error(e)
        );
    });
  }
}
